# NIMHANS_NCR Database - Scripts

## Script Files
| Script               | Intent                       | Note                                                |
| :------------------- | :--------------------------- | :-------------------------------------------------- |
| db-10-init.sql       | Initialize database.         | TODO: Add database options to script.               |
| db-20-create.sql     | Create Tables and relations. | NA                                                  |
| db-21-add-views.sql  | Create Views.                | tbd                                                 |
| db-25-seed.sql       | Insert Seed data.            | tbd, Seed data for application startup.             |
| db-30-drop-views.sql | Remove Views.                | tbd, Clean objects created by `db-21-add-views.sql` |
| db-31-drop.sql       | Remove Tables and relations. | Clean objects created by `db-20-create.sql`         |

## Seed data tables
1. A_DB_INFO
2. GEN_APP_CONFIG
3. DD_CATEGORIES
4. DD_ITEMS

## SQLeoVQB Connection
jdbc:mysql://127.0.0.1:3306/NIMHANS_NCR
ncr-user-rw
fce1a0f1660d4d4da4c096c82ac332ff

