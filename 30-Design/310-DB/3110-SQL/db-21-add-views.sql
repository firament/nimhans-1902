/*
Database Create Script for 'NIMHANS_NCR'

SOURCE LOCATION: $/30-Design/310-DB/3110-SQL/db-21-add-views.sql
Version: 0.0.1.1

Generated by sak on 2019-02-19 16:26:50
*/
CREATE VIEW `VW_CANDIDATE_SCORE_GRADE` AS
SELECT
	CANDIDATES.RD_ID AS RD_ID,
	CANDIDATES.CODE AS C_CODE,
	CANDIDATES.NAME AS C_NAME,
	CANDIDATES.DEPARTMENT AS C_DEPARTMENT,
	CANDIDATES.POST_APPLIED AS C_POST,
	CANDIDATES.AGE AS C_AGE,
	CANDIDATES.CATEGORY AS C_CATEGORY,
	CANDIDATES.STATUS AS C_STATUS,
	SCORES.SCORE AS SCORE,
	SCORES.EDIT_ON AS SCORE_DATE,
	GRADES.GRADE1 AS GRADE,
	GRADES.EDIT_ON AS GRADE_DATE,
	SCORES.USERID AS M_USERID,
	COMMITTEE_MEMBERS.LOGIN AS M_LOGIN,
	COMMITTEE_MEMBERS.NAME AS M_NAME,
	COMMITTEE_MEMBERS.DEPARTMENT AS M_DEPARTMENT,
	COMMITTEE_MEMBERS.ROLE AS M_MEMBERS_ROLE,
	COMMITTEE_MEMBERS.STATUS AS M_STATUS
FROM
	CANDIDATES CANDIDATES
	LEFT OUTER JOIN SCORES SCORES
	 ON CANDIDATES.CODE = SCORES.CODE
	  AND SCORES.RD_ID = CANDIDATES.RD_ID
	LEFT OUTER JOIN GRADES GRADES
	 ON CANDIDATES.CODE = GRADES.CODE
	  AND CANDIDATES.RD_ID = GRADES.RD_ID
	LEFT OUTER JOIN COMMITTEE_MEMBERS COMMITTEE_MEMBERS
	 ON SCORES.USERID = COMMITTEE_MEMBERS.USERID;
CREATE VIEW `VW_GRADE_SUMMARY` AS
SELECT
	SCORES.RD_ID AS RD_ID,
	SCORES.CODE AS CODE,
	sum(SCORES.SCORE) AS TOTAL_SCORE,
	count(SCORES.SCORE) AS COUNT,
	GRADES.GRADE1 AS GRADE,
	SCORES.STATUS AS STATUS
FROM
	GRADES GRADES
	RIGHT OUTER JOIN SCORES SCORES
	 ON GRADES.CODE = SCORES.CODE
GROUP BY
	SCORES.RD_ID,
	SCORES.CODE,
	GRADES.GRADE1,
	SCORES.STATUS;
CREATE VIEW `VW_GRADES` AS
SELECT
	CANDIDATES.RD_ID AS RD_ID,
	CANDIDATES.DEPARTMENT AS DEPARTMENT,
	CANDIDATES.POST_APPLIED AS POST_APPLIED,
	CANDIDATES.CODE AS CODE,
	CANDIDATES.NAME AS NAME,
	CANDIDATES.AGE AS AGE,
	CANDIDATES.CATEGORY AS CATEGORY,
	sum( SCORES.SCORE ) AS TOTAL_SCORE,
	count( SCORES.SCORE ) AS SCORE_COUNT,
	CANDIDATES.STATUS AS STATUS,
	GRADES.GRADE1 AS GRADE
FROM
	CANDIDATES CANDIDATES
	LEFT OUTER JOIN SCORES SCORES
	 ON CANDIDATES.CODE = SCORES.CODE
	LEFT OUTER JOIN GRADES GRADES
	 ON CANDIDATES.CODE = GRADES.CODE
GROUP BY
	CANDIDATES.CODE,
	CANDIDATES.NAME,
	GRADES.GRADE1;
CREATE VIEW `VW_SCORE_SUMMARY` AS
SELECT
	SCORES.CODE,
	count(SCORES.CODE) AS COUNT,
	sum( SCORES.SCORE ) AS TOTAL_SCORE
FROM
	SCORES
GROUP BY
	SCORES.CODE;
CREATE VIEW `VW_SCORES` AS
SELECT
	CANDIDATES.RD_ID AS RD_ID,
	CANDIDATES.CODE AS C_CODE,
	CANDIDATES.NAME AS C_NAME,
	CANDIDATES.AGE AS C_AGE,
	CANDIDATES.CATEGORY AS C_CATEGORY,
	CANDIDATES.DEPARTMENT AS C_DEPARTMENT,
	CANDIDATES.POST_APPLIED AS C_POST_APPLIED,
	CANDIDATES.STATUS AS C_STATUS,
	IFNULL( COMMITTEE_MEMBERS.USERID , 0 ) AS IS_DONE,
	COMMITTEE_MEMBERS.USERID AS M_USERID,
	COMMITTEE_MEMBERS.LOGIN AS M_LOGIN,
	COMMITTEE_MEMBERS.ROLE AS M_ROLE,
	COMMITTEE_MEMBERS.NAME AS M_NAME,
	COMMITTEE_MEMBERS.DEPARTMENT AS M_DEPARTMENT,
	COMMITTEE_MEMBERS.MOBILE AS M_MOBILE,
	COMMITTEE_MEMBERS.EMAIL AS M_EMAIL,
	COMMITTEE_MEMBERS.STATUS AS M_STATUS
FROM
	CANDIDATES CANDIDATES
	LEFT OUTER JOIN SCORES SCORES
	 ON CANDIDATES.CODE = SCORES.CODE
	 AND SCORES.RD_ID = CANDIDATES.RD_ID
	LEFT OUTER JOIN COMMITTEE_MEMBERS COMMITTEE_MEMBERS
	 ON SCORES.USERID = COMMITTEE_MEMBERS.USERID
	 AND COMMITTEE_MEMBERS.RD_ID = SCORES.RD_ID
