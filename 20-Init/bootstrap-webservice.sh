#! /bin/bash
#
#	SOURCE LOCATION: $/20-Init/bootstrap-webservice.sh
#	Version: 0.0.1.1
#	
#	

# Pre-Requisites
#	dotnet core is initialized

##
#	Prepare the project files
##
cd 40-Develop/420-WebService;
dotnet new sln -n ncr_webservice_soln;
#  -n, --name          The name for the output being created. If no name is specified, the name of the current directory is used.
#  -o, --output        Location to place the generated output.
#  --dry-run           Displays a summary of what would happen if the given command line were run if it would result in a template creation.

## Web service instance
mkdir -vp ncrwebservice; cd ncrwebservice;
dotnet new webapi --language "C#"; # -au None;
dotnet add package NLog;
dotnet add package NLog.Web.AspNetCore;
dotnet add package Pomelo.EntityFrameworkCore.MySql
# dotnet remove package MySql.Data --version 8.0.15
# dotnet remove package MySql.Data.EntityFrameworkCore --version 8.0.15
dotnet add package Swashbuckle.AspNetCore;
# dotnet add package Newtonsoft.Json; # Include only if needed
touch README.md;
mkdir -vp Core; 	# for Interfaces and app specific classes
touch Core/README.md;
mkdir -vp Models; 	# for non-persistent models
touch Models/README.md;
touch NLog.config;
cd ..;

## Library of reusable code
mkdir -vp ncr.Common;cd ncr.Common;
dotnet new classlib --language "C#" --framework netcoreapp2.2;
dotnet add package NLog;
# dotnet add package Newtonsoft.Json; # Include only if needed
touch README.md;
mkdir -vp Constants; 	# for constants and enum definitions
touch Constants/README.md;
mkdir -vp Interfaces; 	# for Interfaces used across the solution
touch Interfaces/README.md;
mkdir -vp Utils; 		# for Static Utility functions
touch Utils/README.md;
cd ..;

## ORM Library for DB interface
mkdir -vp ncr.Efc;cd ncr.Efc;
dotnet new classlib --language "C#" --framework netcoreapp2.2;
dotnet add package NLog;
dotnet add package Microsoft.EntityFrameworkCore.Design;
dotnet add package Pomelo.EntityFrameworkCore.MySql;
# dotnet remove package MySql.Data --version 8.0.15
# dotnet remove package MySql.Data.EntityFrameworkCore --version 8.0.15
# dotnet add package Newtonsoft.Json; # Include only if needed
dotnet add package Microsoft.AspNetCore.Mvc;	# for providing HTTP Status Codes.
touch README.md;
mkdir -vp DbFuncs;
touch DbFuncs/README.md;
mkdir -vp EfCustomization/EntityExtensions;
touch EfCustomization/README.md;
touch EfCustomization/EntityExtensions/README.md;
mkdir -vp EntityMetaData;
touch EntityMetaData/README.md;
cd ..;

# Add project references
# dotnet add [<PROJECT>] reference [-f|--framework] <PROJECT_REFERENCES> [-h|--help]
dotnet add ncr.Efc/ncr.Efc.csproj reference ncr.Common/ncr.Common.csproj
dotnet add ncrwebservice/ncrwebservice.csproj reference ncr.Common/ncr.Common.csproj ncr.Efc/ncr.Efc.csproj;

# All all the projects to solution
dotnet sln ncr_webservice_soln.sln add **/*.csproj;

##
#	Scaffold entity framework from existing database
# Pre-Requisites
# 	dotnet add package Microsoft.EntityFrameworkCore.Design
# 	dotnet add package Pomelo.EntityFrameworkCore.MySql
#   DB Scripts run successfull
#       1. $/30-Design/310-DB/3110-SQL/db-10-init.sql
#       2. $/30-Design/310-DB/3110-SQL/db-20-create.sql
##
cd 40-Develop/420-WebService/ncr.Efc;
dotnet ef dbcontext scaffold \
	"server=127.0.0.1;port=3306;user=ncr-user-rw;password=fce1a0f1660d4d4da4c096c82ac332ff;database=NIMHANS_NCR" \
	MySql.Data.EntityFrameworkCore \
	-v -f \
	-d \
	-o dbsets \
	-c NCR_DBContext \
	--framework netcoreapp2.2
#	--use-database-names \

# dotnet ef dbcontext scaffold \
# 	"server=127.0.0.1;port=3306;user=ncr-user-rw;password=fce1a0f1660d4d4da4c096c82ac332ff;database=NIMHANS_NCR" \
# 	Pomelo.EntityFrameworkCore.MySql \
# 	-v -f \
# 	-d \
# 	-o dbsets \
# 	-c NCR_DBContext \
# 	--framework netcoreapp2.2

