# Webservice Deployment

## Prerequisites
- MySQL is installed, with Mixed mode authentication.
- .Net Core 2.2 SDK is installed
  - Verify by running from command line
  - `dotnet --version`

## Setup Webservice
- Create a folder
  - e.g. `C:\WebServices`
  - Note: This path will be used in this document, change the name according to the name you select.
- Get Binaries
  - NCR_WebService-190217.zip
  - from https://drive.google.com/file/d/15u7-bGDN0iaPru1emXWWsbGs7WVwfxK7/view?usp=sharing
- Extract
  - To folder created in earlier step.
  - `C:\WebServices`
  - You should now see a folder `C:\WebServices\NCR_WebService\`
- Configure
  - Open file `C:\WebServices\NCR_WebService\appsettings.json` 
    - in a text editor or in Visual Studio Code
  - Update Database server address and port number, 2 instances.
  - UserIDs and Passwords should not need change if database was created with script.
    - Update UserIDs and Passwords if needed.
- Run
  - Open Terminal (`CMD.exe`)
  - Navigate to path `CD C:\WebServices\NCR_WebService\`
  - launch webservice with command
  - `dotnet ncrwebservice.dll`
- View Documentation and Run
  - Open url `http://localhost:5000/` in browser
    - replace 'localhost' with IP address is accessing from a different machine.
  - From here, you can
  - View the Webservice specification in detail
  - Test each endpoint
  - Download the OpenAPI / Swagger specification json.
