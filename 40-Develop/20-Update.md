# Application Update

## Prerequisites
- Application Deployment is done

## Update Database
> - These steps are for development time only
> - Scripts should be run in sequence, as documented.

- Extract files
  - from zip `30-Design-yymmdd.zip`
  - to any folder e.g. `C:\Temp\Artefacts\`.
  - on success, you should see folder `30-Design`
- Navigate to scripts folder
  - `30-Design\310-DB\3110-SQL\`
- Clean previous deployment, by running scrips
  - **Important:** do NOT do this step in production.
  - db-30-drop-views.sql
  - db-31-drop.sql 
- Update database structure, by running scripts
  - db-20-create.sql
  - db-21-add-views.sql
- Re-Create seed data, by running scripts
  - db-24-seed-data-raw.sql
- Inspect random tables to verify structure and data.


## Update Webservice
- Locate Deployment folder
  - e.g. `C:\WebServices\`
  - Note: This path will be used in this document, change the name according to the name you select.
- Clean earlier Deployment
  - Copy Configuration files, to seperate folder (e.g. `C:\WebServices\bak-yymmdd\`)
    - appsettings.json
    - NLog.config
  - Delete deployment folder (`NCR_WebService` only)
    - `RMDIR /Q /S C:\WebServices\NCR_WebService\`
- Get Binaries
  - NCR_WebService-*yymmdd*.zip
  - from `<public accessible URL or location>`
- Extract
  - To folder created in deployment.
  - `C:\WebServices`
  - You should now see a folder `C:\WebServices\NCR_WebService\`
- Update Configuration
  - Replace configuration files from backup
  - **NOTE:** do NOT drag and drop, ght click and use menu copy + paste
- Run
  - Open Terminal (`CMD.exe`)
  - Navigate to path `CD C:\WebServices\NCR_WebService\`
  - launch webservice with command
  - `dotnet ncrwebservice.dll`
- View Documentation and Run
  - Open url `http://localhost:5000/` in browser
    - replace 'localhost' with IP address is accessing from a different machine.
  - From here, you can
  - View the Webservice specification in detail
  - Test each endpoint
  - Download the OpenAPI / Swagger specification json.
