using System;
using System.ComponentModel.DataAnnotations;
using ncr.Common.Constants;

namespace ncr.Common.Models{

	/// <summary>
	/// Form to submit when requesting data.
	/// </summary>
	public class ListForm
	{
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		[Required]
		public string Ticket { get; set; }
		[Required]
		public ncr.Common.Constants.enListType ListType { get; set; }

		/// <summary>
		/// Recruitment drive ID of which data is to be used.
		/// </summary>
		/// <value></value>
		/// <remarks>Use the value that is got from authentication ticket.</remarks>
		[Required]
		public int RDID { get; set; }	

		/// <summary>
		/// Department Value to filter Posts by.
		/// </summary>
		/// <value>Empty string if not relevant</value>
		public string Department { get; set; }

		/// <summary>
		/// Post to filter Candidates by.
		/// </summary>
		/// <value>Empty string if not relevant</value>
		public string PostAppliedFor { get; set; }

		/// <summary>
		/// Candidate code when requesting a specific candidate details
		/// </summary>
		/// <value></value>
		public string CandidateCode { get; set; }


		/// <summary>
		/// Validate fields in this form.
		/// </summary>
		/// <returns>True, if all rules are satisfied.</returns>
		/// <remarks>TODO: Use Model State to include failure specifics.</remarks>
		public bool IsValid(){
			bool lbValid = false;

			if (string.IsNullOrWhiteSpace(Ticket)) return lbValid;

			switch (ListType)
			{
				// Values for /api/DataList
				case enListType.RecruitmentDrives:
					break;
				case enListType.Departments:
					if (RDID < 1) return lbValid;
					break;
				case enListType.Posts:
					if (RDID < 1 || string.IsNullOrWhiteSpace(Department)) return lbValid;
					break;

				// Values for /api/CandidateList
				case enListType.CandidatesIndividual:
				case enListType.CandidatesSunmarized:
					if (RDID < 1 || string.IsNullOrWhiteSpace(Department) || string.IsNullOrWhiteSpace(PostAppliedFor)) return lbValid;
					break;
				case enListType.CandidatesScoreDetail:
					if (RDID < 1 || string.IsNullOrWhiteSpace(CandidateCode)) return lbValid;
					break;

				default:
					log.Warn("{0} - Unrecognized listType value.");
					return lbValid;
					// break;
			}
			return true;
		}
	}
}
