namespace ncr.Common.Models
{
	/// <summary>
	/// Container for summarized scores.
	/// </summary>
	public class CandidateView
	{
		public int RDID { get; set; }

		// Candidate Details
		public string Code { get; set; }
		public string Name { get; set; }

		// public System.DateTime Age { get; set; }
		/// <summary>
		/// Age in format yy
		/// </summary>
		/// <value></value>
		public int Age { get; set; }

		public string Category { get; set; }

		// Score details
		public decimal Score { get; set; }
		public string ScoreDate { get; set; }
		public decimal TotalScore { get; set; }
		public string Grade { get; set; }
		public string GradeDate { get; set; }

		/// <summary>
		/// Count of examiners that scored the candidate.
		/// </summary>
		/// <value></value>
		public int ScoreCount { get; set; }
		
		// For UI convenience
		public bool IsDone { get; set; }

	}
}
