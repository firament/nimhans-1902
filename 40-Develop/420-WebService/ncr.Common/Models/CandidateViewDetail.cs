namespace ncr.Common.Models
{
	/// <summary>
	/// Container for summarized scores.
	/// </summary>
	/// <remarks>Additional details addes for CR 190219</remarks>
	public class CandidateViewDetail :CandidateView
	{
		// Member Details
		public string MemberLogin { get; set; }
		public string MemberName { get; set; }
	}
}
