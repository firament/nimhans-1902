using System;

namespace ncr.Common.Models
{
	public class AuthTicket
	{
		public int RDID { get; set; }
		public int USERID { get; set; }
		public int Role { get; set; }
		public string Name { get; set; }
		public string Ticket { get; set; }
	}
}
