using System;

namespace ncr.Common.Models
{
	public class UserCreds
	{
		public uint RDID { get; set; }
		public string LOGIN { get; set; }
		public string PASSWORD { get; set; }
	}
}
