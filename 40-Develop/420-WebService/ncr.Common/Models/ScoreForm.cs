using System;
using System.ComponentModel.DataAnnotations;
using ncr.Common.Constants;

namespace ncr.Common.Models{
	/// <summary>
	/// Form to submit when Submitting Score and/or grades.
	/// </summary>
	public class ScoreForm
	{
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		[Required]
		public string Ticket { get; set; }

		/// <summary>
		/// Identifies type of list to be returned.
		/// </summary>
		/// <value>1 = Return list of all Recruitment drives in the system</value>
		/// <value>2 = Return list of all valid Departments that have applications for the given Recruitment Drive</value>
		[Required]
		public ncr.Common.Constants.enListType ListType { get; set; }

		/// <summary>
		/// Recruitment Drive ID of which data is to be used.
		/// </summary>
		/// <value>integer</value>
		/// <remarks>Use the value that is got from authentication ticket.</remarks>
		[Required]
		public int RDID { get; set; }	

		/// <summary>
		/// Code of the Candidate, uniquely identifying the applicant in the system.
		/// </summary>
		/// <value></value>
		public string Code { get; set; }

		/// <summary>
		/// The Score of the specified Candidate
		/// </summary>
		/// <value></value>
		/// <remarks>When ListType = 5, this will the the total score.</remarks>
		public decimal Score { get; set; }

		/// <summary>
		/// The Grade of the specified Candidate
		/// </summary>
		/// <value></value>
		public string Grade { get; set; }


		/// <summary>
		/// Validate fields in this form.
		/// </summary>
		/// <returns>True, if all rules are satisfied.</returns>
		/// <remarks>TODO: Use Model State to include failure specifics.</remarks>
		public bool IsValid()
		{
			bool lbValid = false;

			if (string.IsNullOrWhiteSpace(Ticket) || RDID < 1 || string.IsNullOrWhiteSpace(Code)) return lbValid;

			switch (ListType)
			{
				// Values for /api/Scores
				case enListType.ScoreOnly:
					if (Score <= 0) return lbValid;
					break;
				case enListType.GradeOnly:
					if (string.IsNullOrWhiteSpace(Grade)) return lbValid;
					break;
				case enListType.BothScoreAndGrade:
					if (Score <= 0 || string.IsNullOrWhiteSpace(Grade)) return lbValid;
					break;

				default:
					log.Warn("{0} - Unrecognized listType value.");
					return lbValid;
					// break;
			}
			return true;
		}
	}
}
