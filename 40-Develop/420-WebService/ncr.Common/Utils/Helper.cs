namespace ncr.Common.Utils
{
	public static class Helper
	{
		public static string Token()
		{
			return System.Guid.NewGuid().ToString("D").Replace("-", "");
		}
	}
}
