namespace ncr.Common.Constants{

	/// <summary>
	/// Enumaration to identify the type of request being made
	/// </summary>
	public enum enListType
	{
		// // // For use by /api/DataList

		/// <summary>
		/// Return list of active Recruitment Drives in system
		/// </summary>
		RecruitmentDrives = 1,

		/// <summary>
		/// Return list of all Departments in the system for the given Recruitment Drive.
		/// </summary>
		Departments = 2,

		/// <summary>
		/// Return list of all Posts that have applicants in the system, 
		/// for the given Recruitment Drive and Department.
		/// </summary>
		Posts = 3,

		// // // For use by /api/CandidateList

		/// <summary>
		/// Return list of all Candidates in the system 
		/// for the given Recruitment Drive, Department and Position.
		/// </summary>
		CandidatesIndividual = 10,

		/// <summary>
		/// Return list os all Candidates along with thier summarized Scores and Grades 
		/// for the given Recruitment Drive, Department and Position.
		/// </summary>
		/// <remarks>Requires role of Chairman for this request.</remarks>
		CandidatesSunmarized = 20,

		/// Return list of all Candidates in the system 
		/// for the given Recruitment Drive, Department and Position,
		/// Along with user that gave score
		CandidatesScoreDetail = 30,


		// // // For use by /api/Scores

		ScoreOnly = 100,
		GradeOnly = 200,
		BothScoreAndGrade = 300,
	}
}
