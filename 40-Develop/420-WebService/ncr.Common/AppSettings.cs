﻿using System;

namespace ncr.Common
{
	public interface IAppSettingContainer
	{
        int AppVersion { get; set; }
		string DBConnectionStringRW { get; set; }
		string DBConnectionStringRO { get; set; }
		DateTime LastLoaded { get; set; }
	}

	public class AppSettingContainer:IAppSettingContainer
    {
        public AppSettingContainer()
        {
        }

		public int AppVersion { get; set; }
		public string DBConnectionStringRW { get; set; }
		public string DBConnectionStringRO { get; set; }
		public DateTime LastLoaded { get; set; }
	}
}
