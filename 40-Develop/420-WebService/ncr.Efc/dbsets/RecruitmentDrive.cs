﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ncr.Efc.dbsets
{
    [Table("RECRUITMENT_DRIVE", Schema = "NIMHANS_NCR")]
    public partial class RecruitmentDrive
    {
        public RecruitmentDrive()
        {
            Candidates = new HashSet<Candidates>();
            CommitteeMembers = new HashSet<CommitteeMembers>();
            Grades = new HashSet<Grades>();
            Scores = new HashSet<Scores>();
        }

        [Key]
        [Column("RD_ID", TypeName = "int(10) unsigned")]
        public int RdId { get; set; }
        [Required]
        [Column("NAME")]
        [StringLength(60)]
        public string Name { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [InverseProperty("Rd")]
        public virtual ICollection<Candidates> Candidates { get; set; }
        [InverseProperty("Rd")]
        public virtual ICollection<CommitteeMembers> CommitteeMembers { get; set; }
        [InverseProperty("Rd")]
        public virtual ICollection<Grades> Grades { get; set; }
        [InverseProperty("Rd")]
        public virtual ICollection<Scores> Scores { get; set; }
    }
}
