﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ncr.Efc.dbsets
{
    [Table("COMMITTEE_MEMBERS", Schema = "NIMHANS_NCR")]
    public partial class CommitteeMembers
    {
        public CommitteeMembers()
        {
            Grades = new HashSet<Grades>();
            Scores = new HashSet<Scores>();
        }

        [Key]
        [Column("USERID", TypeName = "int(10) unsigned")]
        public int Userid { get; set; }
        [Column("RD_ID", TypeName = "int(10) unsigned")]
        public int RdId { get; set; }
        [Required]
        [Column("LOGIN")]
        [StringLength(20)]
        public string Login { get; set; }
        [Required]
        [Column("PASSWORD")]
        [StringLength(20)]
        public string Password { get; set; }
        [Column("ROLE", TypeName = "int(11)")]
        public int Role { get; set; }
        [Required]
        [Column("NAME")]
        [StringLength(60)]
        public string Name { get; set; }
        [Required]
        [Column("DEPARTMENT")]
        [StringLength(60)]
        public string Department { get; set; }
        [Required]
        [Column("MOBILE")]
        [StringLength(60)]
        public string Mobile { get; set; }
        [Required]
        [Column("EMAIL")]
        [StringLength(60)]
        public string Email { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("RdId")]
        [InverseProperty("CommitteeMembers")]
        public virtual RecruitmentDrive Rd { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<Grades> Grades { get; set; }
        [InverseProperty("User")]
        public virtual ICollection<Scores> Scores { get; set; }
    }
}
