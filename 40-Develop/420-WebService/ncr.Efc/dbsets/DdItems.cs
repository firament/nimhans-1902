﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ncr.Efc.dbsets
{
    [Table("DD_ITEMS", Schema = "NIMHANS_NCR")]
    public partial class DdItems
    {
        [Column("DDI_CATG_ID", TypeName = "int(10) unsigned")]
        public int DdiCatgId { get; set; }
        [Column("DDI_CODE", TypeName = "int(10) unsigned")]
        public int DdiCode { get; set; }
        [Column("DDI_DISP_SEQ", TypeName = "smallint(5)")]
        public short DdiDispSeq { get; set; }
        [Required]
        [Column("DDI_CODE_TXT")]
        [StringLength(8)]
        public string DdiCodeTxt { get; set; }
        [Required]
        [Column("DDI_DISP_TEXT")]
        [StringLength(24)]
        public string DdiDispText { get; set; }
        [Column("DDI_DESC")]
        [StringLength(255)]
        public string DdiDesc { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("DdiCatgId")]
        [InverseProperty("DdItems")]
        public virtual DdCategories DdiCatg { get; set; }
    }
}
