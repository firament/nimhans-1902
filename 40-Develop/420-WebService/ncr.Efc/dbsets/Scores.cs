﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ncr.Efc.dbsets
{
    [Table("SCORES", Schema = "NIMHANS_NCR")]
    public partial class Scores
    {
        [Column("RD_ID", TypeName = "int(10) unsigned")]
        public int RdId { get; set; }
        [Column("USERID", TypeName = "int(10) unsigned")]
        public int Userid { get; set; }
        [Column("CODE")]
        [StringLength(20)]
        public string Code { get; set; }
        [Column("SCORE", TypeName = "decimal(5,1)")]
        public decimal Score { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("Code")]
        [InverseProperty("Scores")]
        public virtual Candidates CodeNavigation { get; set; }
        [ForeignKey("RdId")]
        [InverseProperty("Scores")]
        public virtual RecruitmentDrive Rd { get; set; }
        [ForeignKey("Userid")]
        [InverseProperty("Scores")]
        public virtual CommitteeMembers User { get; set; }
    }
}
