﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace ncr.Efc.dbsets
{
    public partial class NCR_DBContext : DbContext
    {
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();
		public NCR_DBContext()
        {
        }

        public NCR_DBContext(DbContextOptions<NCR_DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<ADbInfo> ADbInfo { get; set; }
        public virtual DbSet<Candidates> Candidates { get; set; }
        public virtual DbSet<CommitteeMembers> CommitteeMembers { get; set; }
        public virtual DbSet<DdCategories> DdCategories { get; set; }
        public virtual DbSet<DdItems> DdItems { get; set; }
        public virtual DbSet<GenAppConfig> GenAppConfig { get; set; }
        public virtual DbSet<Grades> Grades { get; set; }
        public virtual DbSet<MaintSessions> MaintSessions { get; set; }
        public virtual DbSet<RecruitmentDrive> RecruitmentDrive { get; set; }
        public virtual DbSet<Scores> Scores { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            log.Warn("DB Context is not configured!");
			}
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.2-servicing-10034");

            modelBuilder.Entity<ADbInfo>(entity =>
            {
                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Note).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.Property(e => e.Version).IsUnicode(false);
            });

            modelBuilder.Entity<Candidates>(entity =>
            {
                entity.HasIndex(e => e.RdId)
                    .HasName("FKCANDIDATES924905");

                entity.Property(e => e.Code)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Category).IsUnicode(false);

                entity.Property(e => e.Department).IsUnicode(false);

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.PostApplied).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.Rd)
                    .WithMany(p => p.Candidates)
                    .HasForeignKey(d => d.RdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKCANDIDATES924905");
            });

            modelBuilder.Entity<CommitteeMembers>(entity =>
            {
                entity.HasIndex(e => new { e.RdId, e.Login })
                    .HasName("UNIQUE_MEMBERS")
                    .IsUnique();

                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Department).IsUnicode(false);

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Email)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NA");

                entity.Property(e => e.Login).IsUnicode(false);

                entity.Property(e => e.Mobile)
                    .IsUnicode(false)
                    .HasDefaultValueSql("NA");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Password).IsUnicode(false);

                entity.Property(e => e.Role).HasDefaultValueSql("0");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.Rd)
                    .WithMany(p => p.CommitteeMembers)
                    .HasForeignKey(d => d.RdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKCOMMITTEE_43016");
            });

            modelBuilder.Entity<DdCategories>(entity =>
            {
                entity.Property(e => e.DdiCatgId).ValueGeneratedNever();

                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DdiCatgDesc).IsUnicode(false);

                entity.Property(e => e.DdiCatgName).IsUnicode(false);

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<DdItems>(entity =>
            {
                entity.HasKey(e => new { e.DdiCatgId, e.DdiCode });

                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DdiCodeTxt).IsUnicode(false);

                entity.Property(e => e.DdiDesc).IsUnicode(false);

                entity.Property(e => e.DdiDispText).IsUnicode(false);

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.DdiCatg)
                    .WithMany(p => p.DdItems)
                    .HasForeignKey(d => d.DdiCatgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKDD_ITEMS184240");
            });

            modelBuilder.Entity<GenAppConfig>(entity =>
            {
                entity.HasIndex(e => new { e.AppId, e.AppConfigVer })
                    .HasName("UNIQ_VERSION")
                    .IsUnique();

                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.AppConfig).IsUnicode(false);

                entity.Property(e => e.AppConfigVer).HasDefaultValueSql("1");

                entity.Property(e => e.AppId).IsUnicode(false);

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Grades>(entity =>
            {
                entity.HasKey(e => new { e.RdId, e.Code });

                entity.HasIndex(e => e.Code)
                    .HasName("FKGRADES333386");

                entity.HasIndex(e => e.Userid)
                    .HasName("FKGRADES634099");

                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Grade1).IsUnicode(false);

                entity.Property(e => e.Grade2).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.CodeNavigation)
                    .WithMany(p => p.Grades)
                    .HasForeignKey(d => d.Code)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKGRADES333386");

                entity.HasOne(d => d.Rd)
                    .WithMany(p => p.Grades)
                    .HasForeignKey(d => d.RdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKGRADES698974");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Grades)
                    .HasForeignKey(d => d.Userid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKGRADES634099");
            });

            modelBuilder.Entity<MaintSessions>(entity =>
            {
                entity.Property(e => e.DeviceSig).IsUnicode(false);

                entity.Property(e => e.IssueTime).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.LastUsed).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Role).HasDefaultValueSql("1");

                entity.Property(e => e.Token).IsUnicode(false);
            });

            modelBuilder.Entity<RecruitmentDrive>(entity =>
            {
                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Status).HasDefaultValueSql("0");
            });

            modelBuilder.Entity<Scores>(entity =>
            {
                entity.HasKey(e => new { e.RdId, e.Userid, e.Code });

                entity.HasIndex(e => e.Code)
                    .HasName("FKSCORES461241");

                entity.HasIndex(e => e.Userid)
                    .HasName("FKSCORES209636");

                entity.Property(e => e.Code).IsUnicode(false);

                entity.Property(e => e.AddBy).HasDefaultValueSql("0");

                entity.Property(e => e.AddOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EditBy).HasDefaultValueSql("0");

                entity.Property(e => e.EditOn).HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Score).HasDefaultValueSql("0.0");

                entity.Property(e => e.Status).HasDefaultValueSql("0");

                entity.HasOne(d => d.CodeNavigation)
                    .WithMany(p => p.Scores)
                    .HasForeignKey(d => d.Code)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKSCORES461241");

                entity.HasOne(d => d.Rd)
                    .WithMany(p => p.Scores)
                    .HasForeignKey(d => d.RdId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKSCORES826829");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.Scores)
                    .HasForeignKey(d => d.Userid)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FKSCORES209636");
            });
        }
    }
}
