﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ncr.Efc.dbsets
{
    [Table("GEN_APP_CONFIG", Schema = "NIMHANS_NCR")]
    public partial class GenAppConfig
    {
        [Column("ID", TypeName = "int(10) unsigned")]
        public int Id { get; set; }
        [Required]
        [Column("APP_ID")]
        [StringLength(8)]
        public string AppId { get; set; }
        [Column("APP_CONFIG_VER", TypeName = "int(5)")]
        public int AppConfigVer { get; set; }
        [Required]
        [Column("APP_CONFIG")]
        public string AppConfig { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }
    }
}
