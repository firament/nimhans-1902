﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ncr.Efc.dbsets
{
    [Table("GRADES", Schema = "NIMHANS_NCR")]
    public partial class Grades
    {
        [Column("RD_ID", TypeName = "int(10) unsigned")]
        public int RdId { get; set; }
        [Column("CODE")]
        [StringLength(20)]
        public string Code { get; set; }
        [Column("USERID", TypeName = "int(10) unsigned")]
        public int Userid { get; set; }
        [Required]
        [Column("GRADE1")]
        [StringLength(3)]
        public string Grade1 { get; set; }
        [Column("GRADE2")]
        [StringLength(3)]
        public string Grade2 { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("Code")]
        [InverseProperty("Grades")]
        public virtual Candidates CodeNavigation { get; set; }
        [ForeignKey("RdId")]
        [InverseProperty("Grades")]
        public virtual RecruitmentDrive Rd { get; set; }
        [ForeignKey("Userid")]
        [InverseProperty("Grades")]
        public virtual CommitteeMembers User { get; set; }
    }
}
