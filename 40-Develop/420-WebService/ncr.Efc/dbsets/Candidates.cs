﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ncr.Efc.dbsets
{
    [Table("CANDIDATES", Schema = "NIMHANS_NCR")]
    public partial class Candidates
    {
        public Candidates()
        {
            Grades = new HashSet<Grades>();
            Scores = new HashSet<Scores>();
        }

        [Key]
        [Column("CODE")]
        [StringLength(20)]
        public string Code { get; set; }
        [Column("RD_ID", TypeName = "int(10) unsigned")]
        public int RdId { get; set; }
        [Required]
        [Column("NAME")]
        [StringLength(60)]
        public string Name { get; set; }
        [Required]
        [Column("DEPARTMENT")]
        [StringLength(60)]
        public string Department { get; set; }
        [Required]
        [Column("POST_APPLIED")]
        [StringLength(60)]
        public string PostApplied { get; set; }
        [Column("AGE", TypeName = "int(2)")]
        public int Age { get; set; }
        [Required]
        [Column("CATEGORY")]
        [StringLength(20)]
        public string Category { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int Status { get; set; }
        [Column("ADD_BY", TypeName = "int(10) unsigned")]
        public int AddBy { get; set; }
        [Column("ADD_ON")]
        public DateTimeOffset AddOn { get; set; }
        [Column("EDIT_BY", TypeName = "int(10) unsigned")]
        public int EditBy { get; set; }
        [Column("EDIT_ON")]
        public DateTimeOffset EditOn { get; set; }

        [ForeignKey("RdId")]
        [InverseProperty("Candidates")]
        public virtual RecruitmentDrive Rd { get; set; }
        [InverseProperty("CodeNavigation")]
        public virtual ICollection<Grades> Grades { get; set; }
        [InverseProperty("CodeNavigation")]
        public virtual ICollection<Scores> Scores { get; set; }
    }
}
