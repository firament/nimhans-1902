﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ncr.Efc.dbsets
{
    [Table("MAINT_SESSIONS", Schema = "NIMHANS_NCR")]
    public partial class MaintSessions
    {
        [Key]
        [Column("MSID", TypeName = "int(10) unsigned")]
        public int Msid { get; set; }
        [Required]
        [Column("TOKEN")]
        [StringLength(60)]
        public string Token { get; set; }
        [Column("DEVICE_SIG")]
        public string DeviceSig { get; set; }
        [Column("USERID", TypeName = "int(10) unsigned")]
        public int Userid { get; set; }
        [Column("ROLE", TypeName = "int(11)")]
        public int Role { get; set; }
        [Column("ISSUE_TIME")]
        public DateTimeOffset IssueTime { get; set; }
        [Column("LAST_USED")]
        public DateTimeOffset LastUsed { get; set; }
        [Column("STATUS", TypeName = "int(11)")]
        public int? Status { get; set; }
    }
}
