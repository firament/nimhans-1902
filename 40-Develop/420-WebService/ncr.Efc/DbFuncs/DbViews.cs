using System;
using System.Collections.Generic;
using D = MySql.Data;
using DC = MySql.Data.MySqlClient;
using ncr.Common;
using ncr.Common.Models;
using NLog;
using Microsoft.AspNetCore.Mvc;

namespace ncr.Efc.DbFuncs
{
	public static class DbViews
	{
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		// [Obsolete("To update and replace current RW call.", true)]
		/// <summary>
		/// List of Candidates for member, with flag to indicate if already scored.
		/// </summary>
		/// <param name="UserID"></param>
		/// <param name="AppConfig"></param>
		/// <param name="ListFilter"></param>
		/// <returns></returns>
		public static (StatusCodeResult Status, List<CandidateViewDetail> DataList) 
				CandidateListForMember(int UserID, IAppSettingContainer AppConfig, ListForm ListFilter)
		{
			// No Validations, assumin call will have scrubbed data for no.
			StatusCodeResult loStatus = null;
			string connStr = AppConfig.DBConnectionStringRO;
			List<CandidateViewDetail> lstEntries = new List<CandidateViewDetail>();
			Dictionary<string, bool> ldyScoreStatus = new Dictionary<string, bool>();
			try
			{

				using (DC.MySqlConnection dbConn = new DC.MySqlConnection(connStr))
				{
					DC.MySqlDataReader dbRDR;
					DC.MySqlCommand dbCMD;
					string lsCandCode;
					string lsCMD;

					dbConn.Open();
					log.Debug("Connection status = {0}", dbConn.State);

					// TODO: get command from constants
					/*
					SELECT CODE, RD_ID, NAME, DEPARTMENT, POST_APPLIED, AGE, CATEGORY, STATUS
					FROM CANDIDATES 
					WHERE STATUS = 1 AND DEPARTMENT = @DEPARTMENT AND POST_APPLIED = @POST_APPLIED;

					SELECT C_CODE 
					FROM VW_SCORES
					WHERE C_STATUS = 1 AND M_STATUS = 1 AND C_DEPARTMENT = @C_DEPARTMENT AND C_POST_APPLIED = @C_POST_APPLIED AND M_USERID = @M_USERID
					*/

					//// Get list of candidates that have been scored
					lsCMD = @"SELECT C_CODE 
							  FROM VW_SCORES
							  WHERE C_STATUS = @C_STATUS 
							    AND M_STATUS = @M_STATUS 
								AND RD_ID = @RD_ID 
								AND C_DEPARTMENT = @C_DEPARTMENT 
								AND C_POST_APPLIED = @C_POST_APPLIED 
								AND M_USERID = @M_USERID";

					dbCMD = new DC.MySqlCommand();
					dbCMD.Connection = dbConn;
					// dbCMD.CommandType = 
					dbCMD.CommandText = lsCMD;
					dbCMD.Prepare();

					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@C_STATUS", DC.MySqlDbType.Int32)
					{
						Value = 1
					});
					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@M_STATUS", DC.MySqlDbType.Int32)
					{
						Value = 1
					});
					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@RD_ID", DC.MySqlDbType.Int32)
					{
						Value = ListFilter.RDID
					});
					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@C_DEPARTMENT", DC.MySqlDbType.VarChar)
					{
						Value = ListFilter.Department
					});
					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@C_POST_APPLIED", DC.MySqlDbType.VarChar)
					{
						Value = ListFilter.PostAppliedFor
					});
					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@M_USERID", DC.MySqlDbType.Int32)
					{
						Value = UserID
					});

					dbRDR = dbCMD.ExecuteReader();
					while (dbRDR.Read())
					{
						ldyScoreStatus.Add(dbRDR["C_CODE"].ToString(), true);
					}
					dbRDR.Close();
					log.Info("{0} Candidataes found scored by Member {1}", ldyScoreStatus.Count, UserID);



					//// Get list of all candidates for given department and post
					lsCMD = @"SELECT CODE, RD_ID, NAME, AGE, CATEGORY, STATUS
 							  FROM CANDIDATES 
 							  WHERE STATUS = @STATUS 
								AND RD_ID = @RD_ID 
							    AND DEPARTMENT = @DEPARTMENT 
								AND POST_APPLIED = @POST_APPLIED";

					dbCMD = new DC.MySqlCommand();
					dbCMD.Connection = dbConn;
					// dbCMD.CommandType = 
					dbCMD.CommandText = lsCMD;
					dbCMD.Prepare();

					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@STATUS", DC.MySqlDbType.Int32)
					{
						Value = 1
					});
					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@RD_ID", DC.MySqlDbType.Int32)
					{
						Value = ListFilter.RDID
					});
					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@DEPARTMENT", DC.MySqlDbType.VarChar)
					{
						Value = ListFilter.Department
					});
					dbCMD.Parameters.Add(new MySql.Data.MySqlClient.MySqlParameter("@POST_APPLIED", DC.MySqlDbType.VarChar)
					{
						Value = ListFilter.PostAppliedFor
					});

					dbRDR = dbCMD.ExecuteReader();
					while (dbRDR.Read())
					{
						lsCandCode = dbRDR["CODE"].ToString();
						lstEntries.Add(new CandidateViewDetail()
						{
							RDID = int.Parse(dbRDR["RD_ID"].ToString()),
							Code = lsCandCode,
							Name = dbRDR["NAME"].ToString(),
							Age = int.Parse(dbRDR["AGE"].ToString()),
							Category = dbRDR["CATEGORY"].ToString(),
							IsDone = ldyScoreStatus.ContainsKey(lsCandCode),
							// Score = 0,
							// ScoreDate = null,
							// TotalScore = 0,
							// Grade = "",
							// GradeDate = null,
							// ScoreCount = 0,
							// MemberLogin = "",
							// MemberName = "",
						});
					}
					dbRDR.Close();
					dbConn.Close();
				}

			}
			catch (System.Exception eX)
			{
				log.Error(eX, "Error fetching candidate list for user");
				loStatus = new StatusCodeResult(500);
			}
			return (loStatus, lstEntries);
		}

		/// <summary>
		/// List of candidataes with Summarized scores and Grades assigned
		/// With flag to indicate grading status.
		/// </summary>
		/// <param name="UserID"></param>
		/// <param name="Role"></param>
		/// <param name="AppConfig"></param>
		/// <param name="ListFilter"></param>
		/// <returns></returns>
		public static (StatusCodeResult Status, List<CandidateViewDetail> DataList) 
				CandidateListForChairman(int UserID, int Role, IAppSettingContainer AppConfig, ListForm ListFilter)
		{
			// No Validations, assumin call will have scrubbed data for no.
			StatusCodeResult loStatus = null;
			List<CandidateViewDetail> lstEntries = new List<CandidateViewDetail>();
			string connStr = AppConfig.DBConnectionStringRO;
			try
			{
				using (DC.MySqlConnection dbConn = new DC.MySqlConnection(connStr))
				{
					dbConn.Open();
					log.Debug("Connection status = {0}", dbConn.State);

					// TODO: get command from constants
					string lsCMD = @"SELECT RD_ID, DEPARTMENT, POST_APPLIED, CODE, NAME, AGE, CATEGORY, TOTAL_SCORE, SCORE_COUNT, STATUS, GRADE
									 FROM VW_GRADES
									 WHERE RD_ID = @RD_ID AND STATUS = @STATUS AND DEPARTMENT = @DEPARTMENT AND POST_APPLIED = @POST_APPLIED";

					DC.MySqlCommand dbCMD = new DC.MySqlCommand();
					dbCMD.Connection = dbConn;
					// dbCMD.CommandType = 
					dbCMD.CommandText = lsCMD;
					dbCMD.Prepare();

					dbCMD.Parameters.Add(new DC.MySqlParameter("@STATUS", DC.MySqlDbType.Int32)
					{
						Value = 1
					});
					dbCMD.Parameters.Add(new DC.MySqlParameter("@RD_ID", DC.MySqlDbType.Int32)
					{
						Value = ListFilter.RDID
					});

					dbCMD.Parameters.Add(new DC.MySqlParameter("@DEPARTMENT", DC.MySqlDbType.VarChar)
					{
						Value = ListFilter.Department
					});
					dbCMD.Parameters.Add(new DC.MySqlParameter("@POST_APPLIED", DC.MySqlDbType.VarChar)
					{
						Value = ListFilter.PostAppliedFor
					});

					DC.MySqlDataReader dbRDR = dbCMD.ExecuteReader();
					string lsGrade;
					while (dbRDR.Read())
					{
						lsGrade = dbRDR["GRADE"].ToString();
						lstEntries.Add(new CandidateViewDetail()
						{
							RDID = int.Parse(dbRDR["RD_ID"].ToString()),
							Code = dbRDR["CODE"].ToString(),
							Name = dbRDR["NAME"].ToString(),
							Age  = int.Parse(dbRDR["AGE"].ToString()), // (dbRDR["AGE"].GetType().Name == "DBNull") ? "null" : ((System.DateTime)dbRDR["AGE"]).ToString("yy MM dd"),
							Category = dbRDR["CATEGORY"].ToString(),
							ScoreCount = int.Parse(dbRDR["SCORE_COUNT"].ToString()),
							Score = (dbRDR["TOTAL_SCORE"].GetType().Name == "DBNull") ? 0 : Decimal.Parse(dbRDR["TOTAL_SCORE"].ToString()),
							TotalScore = (dbRDR["TOTAL_SCORE"].GetType().Name == "DBNull") ? 0 : Decimal.Parse(dbRDR["TOTAL_SCORE"].ToString()),
							Grade = lsGrade,
							IsDone = !string.IsNullOrWhiteSpace(lsGrade),
						});
					}
					dbRDR.Close();
					dbConn.Close();
					if (lstEntries.Count == 0) loStatus = new NoContentResult();
				}
			}
			catch (System.Exception eX)
			{
				log.Error(eX, "Error fetching candidate list for user {0}", UserID);
				loStatus = new StatusCodeResult(500);
			}
			return (loStatus, lstEntries);
		}

		/// <summary>
		/// Scoring details of an individual candidate, and Grade assigned
		/// </summary>
		/// <param name="UserID">User making the call</param>
		/// <param name="Role">Role of user making the call</param>
		/// <param name="AppConfig"></param>
		/// <param name="ListFilter"></param>
		/// <returns></returns>
		public static (StatusCodeResult Status, List<CandidateViewDetail> DataList) 
				CandidatesScoreDetail(int UserID, int Role, IAppSettingContainer AppConfig, ListForm ListFilter){
			// No Validations, assumin call will have scrubbed data for no.
			StatusCodeResult loStatus = null;
			List<CandidateViewDetail> lstEntries = new List<CandidateViewDetail>();
			string connStr = AppConfig.DBConnectionStringRO;
			try
			{
				using (DC.MySqlConnection dbConn = new DC.MySqlConnection(connStr))
				{
					dbConn.Open();
					log.Debug("Connection status = {0}", dbConn.State);

					// TODO: get command from constants
					string lsCMD = @"SELECT RD_ID
								         , C_CODE, C_NAME, C_DEPARTMENT, C_POST, C_AGE, C_CATEGORY, C_STATUS
								         , SCORE, SCORE_DATE, GRADE, GRADE_DATE
								         , M_USERID, M_LOGIN, M_NAME, M_DEPARTMENT, M_MEMBERS_ROLE, M_STATUS
							         FROM VW_CANDIDATE_SCORE_GRADE
									 WHERE RD_ID = @RD_ID AND C_CODE = @C_CODE";

					DC.MySqlCommand dbCMD = new DC.MySqlCommand();
					dbCMD.Connection = dbConn;
					// dbCMD.CommandType = 
					dbCMD.CommandText = lsCMD;
					dbCMD.Prepare();

					dbCMD.Parameters.Add(new DC.MySqlParameter("@RD_ID", DC.MySqlDbType.Int32)
					{
						Value = ListFilter.RDID
					});
					dbCMD.Parameters.Add(new DC.MySqlParameter("@C_CODE", DC.MySqlDbType.VarChar)
					{
						Value = ListFilter.CandidateCode
					});

					DC.MySqlDataReader dbRDR = dbCMD.ExecuteReader();
					string lsGrade;
					Decimal ldScore = 0, ldTotalScore = 0;
					while (dbRDR.Read())
					{
						lsGrade = (dbRDR["GRADE"].GetType().Name == "DBNull") ? "" : dbRDR["GRADE"].ToString();
						ldScore = (dbRDR["SCORE"].GetType().Name == "DBNull") ? 0 : Decimal.Parse(dbRDR["SCORE"].ToString());
						if (ldScore > 0) ldTotalScore += ldScore;
						lstEntries.Add(new CandidateViewDetail()
						{
							RDID = int.Parse(dbRDR["RD_ID"].ToString()),
							Code = dbRDR["C_CODE"].ToString(),
							Name = dbRDR["C_NAME"].ToString(),
							Age = int.Parse(dbRDR["C_AGE"].ToString()),
							Category = dbRDR["C_CATEGORY"].ToString(),
							Score = ldScore,
							ScoreDate = (dbRDR["SCORE_DATE"].GetType().Name == "DBNull") ? "" : DateTime.Parse(dbRDR["SCORE_DATE"].ToString()).ToString("yy-MMM-dd HH:mm"),
							Grade = lsGrade,
							GradeDate = (dbRDR["GRADE_DATE"].GetType().Name == "DBNull") ? "" : DateTime.Parse(dbRDR["GRADE_DATE"].ToString()).ToString("yy-MMM-dd HH:mm"),
							MemberLogin = (dbRDR["M_LOGIN"].GetType().Name == "DBNull") ? "NA" : dbRDR["M_LOGIN"].ToString(),
							MemberName = (dbRDR["M_NAME"].GetType().Name == "DBNull") ? "NA" : dbRDR["M_NAME"].ToString(),
							IsDone = !string.IsNullOrWhiteSpace(lsGrade),
						});
					}
					dbRDR.Close();
					dbConn.Close();
					if (lstEntries.Count == 0) loStatus = new NoContentResult();

					// Update Total Score in all entries, consumer can use value from anywhere
					foreach (CandidateViewDetail vCand in lstEntries)
					{
						vCand.TotalScore = ldTotalScore;
					}
				}
			}
			catch (System.Exception eX)
			{
				log.Error(eX, "Error fetching candidate list for user {0}");
				loStatus = new StatusCodeResult(500);
			}
			return (loStatus, lstEntries);
		}

	}
}
