using System;
using System.Collections.Generic;
using D = MySql.Data;
using DC = MySql.Data.MySqlClient;

using ncr.Common.Models;

namespace ncr.Efc.DbFuncs{
	public static class DBActions
	{
		internal readonly static NLog.ILogger log = NLog.LogManager.GetCurrentClassLogger();

		[Obsolete("Use EF version of this, till this version is optimized for consurrency.")]
		public static bool ClearSessions(string ConStr, int UserID){
			// TODO: use transaction.
			int liRows = 0;
			using (DC.MySqlConnection conn = new DC.MySqlConnection(ConStr))
			{
				MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand();
				cmd.Connection = conn;
				cmd.CommandText = @"UPDATE MAINT_SESSIONS WHERE STATUS = 1 AND USERID = @USERID SET STATUS = 2";
				cmd.Prepare();
				cmd.Parameters.AddWithValue("@USERID", UserID);
				liRows = cmd.ExecuteNonQuery();
				conn.Close();
			}
			return (liRows == 0) ? false : true;
		}
	}
}
