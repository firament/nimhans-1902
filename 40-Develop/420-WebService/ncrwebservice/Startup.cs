﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

// using NJsonSchema;
// using NSwag.AspNetCore;
using ncr.Efc.dbsets;
using Swashbuckle.AspNetCore.Swagger;
using Microsoft.EntityFrameworkCore;
// using Microsoft.EntityFrameworkCore.Metadata;
using Pomelo.EntityFrameworkCore;
using System.IO;
using System.Reflection;

namespace ncrwebservice
{
	public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			// Prepare db context
			string lsConnStrRW = Configuration.GetSection("AppSettings:DBConnectionStringRW").Value;
			services.AddSingleton<ncr.Common.IAppSettingContainer>(new ncr.Common.AppSettingContainer()
			{
				AppVersion = 1,
				LastLoaded = DateTime.Now,
				DBConnectionStringRW = lsConnStrRW,
				DBConnectionStringRO = Configuration.GetSection("AppSettings:DBConnectionStringRO").Value,
			});

			services.AddDbContext<ncr.Efc.dbsets.NCR_DBContext>(options =>
				options.UseMySql(lsConnStrRW)
			);
			/*
			services.AddDbContext<ncr.Efc.dbsets.NCR_DBContext>(options =>
				options.UseMySql(lsConnStr, mySqlOptions =>
				{
					mySqlOptions.ServerVersion(new Version(5, 7, 17), ServerType.MySql);
				})
			);
			*/

			services
				.AddMvc()
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_2)
				.ConfigureApiBehaviorOptions(options =>
				{
					// TODO: Apply only required options
				})
				;

			// Register the Swagger generator, defining 1 or more Swagger documents
			services
				.AddSwaggerGen(c =>
				{
					c.SwaggerDoc("nws", new Info { Title = "Nimhans Web Services", Version = "v1" });
					// c.SwaggerDoc("bws", new Info { Title = "BME Web Services C", Version = "v1" });

					// Set the comments path for the Swagger JSON and UI.
					var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
					var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
					c.IncludeXmlComments(xmlPath);
				});

			/*
			// Register the Swagger generator, defining 1 or more Swagger documents
			services.AddSwaggerGen(c =>
			{
				c.SwaggerDoc("v1", new Info
				{
					Version = "v1",
					Title = "Nimhans Web Services",
					Description = "A simple example ASP.NET Core Web API",
					TermsOfService = "None",
					Contact = new Contact
					{
						Name = "R Talluri",
						Email = string.Empty,
						Url = "https://twitter.com/rtalluri"
					},
					License = new License
					{
						Name = "Use under LICX",
						Url = "https://example.com/license"
					}
				});
			});
            */

		}

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IHostingEnvironment env)
		{
			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}
			else
			{
				// The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
				// app.UseHsts();
			}

			app.UseStaticFiles();

			// Enable middleware to serve generated Swagger as a JSON endpoint.
			app.UseSwagger();

			// Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
			// specifying the Swagger JSON endpoint.
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/nws/swagger.json", "NCR Webservice");
				// c.SwaggerEndpoint("/swagger/bws/swagger.json", "BME Webservice");
				c.RoutePrefix = string.Empty;
			});

			app.UseMvc();
			/* Copied from web app
			app.UseMvc(routes =>
			{
				routes.MapRoute(
					name: "default",
					template: "{controller=Home}/{action=Index}/{id?}");
			});
            */
		}
	}
}
