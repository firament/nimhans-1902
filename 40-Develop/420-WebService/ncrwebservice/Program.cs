﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog.Web;

namespace ncrwebservice
{
	public class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("CWD = " + Environment.CurrentDirectory);
			// NLog: setup the logger first to catch any errors
			string lsNlogConfog =
					(
					Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")
						== EnvironmentName.Development
					)
						? "NLog.Development.config"
						: "NLog.config"
						;
			// NLog: Initialize and Configure factory, manager
			NLog.Logger logger = NLog.Web.NLogBuilder
									.ConfigureNLog(lsNlogConfog)
									.GetCurrentClassLogger()
									;
			try
			{
				logger.Info("init main => public static void Main(string[] args)");
				logger.Info("Using log file {0}", lsNlogConfog);
				CreateWebHostBuilder(args).Build().Run();
			}
			catch (Exception ex)
			{
				logger.Error(ex, "App unexpected exception. Quitting.");
				throw;
			}
			finally
			{
				logger.Info("exit main => public static void Main(string[] args)");
				// NLog: Ensure to flush and stop internal timers/threads before application-exit 
				// (Avoid segmentation fault on Linux)
				NLog.LogManager.Shutdown();
			}
		}

		public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
			WebHost.CreateDefaultBuilder(args)
				.UseStartup<Startup>().ConfigureLogging(logging =>
				{
					logging.ClearProviders();
					logging.SetMinimumLevel(Microsoft.Extensions.Logging.LogLevel.Trace);
				})
				.UseNLog()  // NLog: setup NLog for Dependency injection
				// .UseUrls("http://*:5000")
				;
		// for more options, see
		// https://docs.microsoft.com/en-us/aspnet/core/fundamentals/host/web-host?view=aspnetcore-2.2#set-up-a-host
	}
}
