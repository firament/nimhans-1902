using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ncr.Efc;
using ncr.Efc.dbsets;
using ncr.Common;
using ncr.Common.Constants;
using ncr.Common.Models;

namespace ncrwebservice.Controllers{

	/// <summary>
	/// Generalized endpoint to fetch lists of entiies
	/// </summary>
	/// <remarks>Entries returned and Type of entries are controlled by value of ListFilter.listType</remarks>
	[Route("api/[controller]")]
	[Produces("application/json")]
	[ApiController]
	public class DataListController: NCR_Controller
	{
		#region Constructors
		public DataListController(ILogger<DataListController> logger, IAppSettingContainer App_Config, NCR_DBContext db_context) 
		: base(logger, App_Config, db_context)
		{ }
		#endregion // Constructors

		#region GET
		/// <summary>
		/// Get list of values.
		/// </summary>
		/// <param name="ListFilter">
		/// Form with data specifying list type required, and aupporting data. 
		/// Field values have contextual meaning depending on value of <c>listType</c>
		/// 
		/// Valid values and usage for <c>ListFilter.listType</c> are: 
		///     1 = RecruitmentDrives, result will be in format 'RDID - Recruitment Drive Name'
		///     2 = Departments, 
		///     3 = 'Posts applied for', 
		/// </param>
		/// <returns>Valid values in system, of type requested by <c>ListFilter.listType</c></returns>
		/// <response code="400">Submitted form data failed validation.</response>
		/// <response code="401">Authorization Ticket failed check.</response>
		/// <response code="204">No results found for given criteria.</response>
		/// <response code="200">Success. Data requested will be in response.</response>
		/// <remarks>
		/// </remarks>
		[HttpPost]
		[ProducesResponseType(400)]
		[ProducesResponseType(typeof(ListForm), 401)]
		[ProducesResponseType(typeof(ListForm), 204)]
		[ProducesResponseType(typeof(List<string>), 200)]
		public ActionResult GetResult([FromBody]ListForm ListFilter){
			if (!ListFilter.IsValid())
			{
				log.LogWarning("Validation failed for request. {0}", ListFilter);
				return new BadRequestObjectResult(null);
			}
			if (!CheckSession(ListFilter.Ticket))
			{
				log.LogWarning("Authorization failed for request. {0}", ListFilter);
				return new UnauthorizedResult();
			}
			(StatusCodeResult Status, List<string> DataList) CallT;
			switch (ListFilter.ListType)
			{

				case enListType.RecruitmentDrives:
					// return (CallT.Status == null) ? Ok(CallT.DataList) : (StatusCodeResult)CallT.Status;
					CallT = RecruitmentDrives();
					if (CallT.Status == null) { return Ok(CallT.DataList); }
					else { return CallT.Status; }

				case enListType.Departments:
					// return Ok(Departments(ListFilter.RDID));
					CallT = Departments(ListFilter.RDID);
					if (CallT.Status == null) { return Ok(CallT.DataList); }
					else { return CallT.Status; }

				case enListType.Posts:
					//return Ok(Posts(ListFilter.RDID, ListFilter.Department));
					CallT = Posts(ListFilter.RDID, ListFilter.Department);
					if (CallT.Status == null) { return Ok(CallT.DataList); }
					else { return CallT.Status; }

				default:
					return new BadRequestObjectResult(ListFilter);

			}
		}
		#endregion // GET

		#region Helper Methods

		private (StatusCodeResult Status, List<string> DataList) RecruitmentDrives()
		{
			StatusCodeResult loStatus = null;
			List<string> lstItems = new List<string>();
			try
			{
				List<RecruitmentDrive> loRDs = dbcontext.RecruitmentDrive.Where(f => f.Status == 1).ToList();
				log.LogInformation("Found {0} active RecruitmentDrive", loRDs.Count);
				if (loRDs.Count != 1)
				{
					log.LogError("Found {0} RecruitmentDrive, Expected 1.", loRDs.Count);
				}
				foreach (RecruitmentDrive vRD in loRDs)
				{
					lstItems.Add(string.Format("{0} - {1}", vRD.RdId, vRD.Name));
				}
				if (lstItems.Count == 0) loStatus = new NoContentResult();
			}
			catch (System.Exception ex)
			{
				log.LogError(ex, "Error retrieving RecruitmentDrives.");
				loStatus = StatusCode(500);
			}
			return (loStatus, lstItems);
		}

		private (StatusCodeResult Status, List<string> DataList) Departments(int RDID)
		{
			StatusCodeResult loStatus = null;
			List<string> lstItems = new List<string>();
			try
			{
				lstItems = dbcontext.Candidates
						.Where(f => f.RdId == RDID)
						.Where(f => f.Status == 1)
						.Select(f => f.Department)
						.Distinct()
						.ToList();
				log.LogInformation("Found {0} active Department", lstItems.Count);
				if (lstItems.Count == 0) loStatus = new NoContentResult();
			}
			catch (System.Exception ex)
			{
				log.LogError(ex, "Error retrieving Departments.");
				loStatus = StatusCode(500);
			}
			return (loStatus, lstItems);
		}

		private (StatusCodeResult Status, List<string> DataList) Posts(int piRDID, string psDepartment)
		{
			StatusCodeResult loStatus = null;
			List<string> lstItems = new List<string>();
			try
			{
				lstItems = dbcontext.Candidates
						.Where(f => f.RdId == piRDID && f.Department == psDepartment && f.Status == 1)
						.Select(f => f.PostApplied)
						.Distinct()
						.ToList();
				log.LogInformation("Found {0} active Posts", lstItems.Count);
				if (lstItems.Count == 0) loStatus = new NoContentResult();
			}
			catch (System.Exception ex)
			{
				log.LogError(ex, "Error retrieving Posts.");
				loStatus = StatusCode(500);
			}
			return (loStatus, lstItems);
		}

		#endregion //Helper Methods

	}
}
