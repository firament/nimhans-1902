using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ncr.Efc;
using ncr.Efc.dbsets;
using ncr.Common;
using ncr.Common.Models;

namespace ncrwebservice.Controllers{

	[ApiController]
	[Produces("application/json")]
	public class NCR_Controller : ControllerBase
	{
		// Containers and Services from Dependency Injection
		internal readonly ILogger<NCR_Controller> log;
		internal readonly ncr.Common.IAppSettingContainer appconfig;
		internal ncr.Efc.dbsets.NCR_DBContext dbcontext;
		internal MaintSessions UserSession;

		public NCR_Controller() : base(){}
		public NCR_Controller(ILogger<NCR_Controller> logger, IAppSettingContainer App_Config, NCR_DBContext db_context) : base()
		{
			log = logger;
			dbcontext = db_context;
			appconfig = App_Config;
		}

		// Validate and get user session ticket.
		internal bool CheckSession(string psTicket)
		{
			// TODO: Wrap in try block
			List<MaintSessions> loSessions = dbcontext.MaintSessions.Where(f => f.Token == psTicket && f.Status == 1).ToList();
			int liCount = loSessions.Count;
			if (liCount != 1)
			{
				log.LogWarning($"Expected 1, but got {liCount} records for ticket {psTicket}");
				return false;
			}
			// detach from context before using.
			// UserSession = loSessions[0];
			UserSession = new MaintSessions()
			{
				Token = loSessions[0].Token,
				DeviceSig = loSessions[0].DeviceSig,
				Userid = loSessions[0].Userid,
				Role = loSessions[0].Role,
				Status = loSessions[0].Status,
				IssueTime = loSessions[0].IssueTime,
				LastUsed = loSessions[0].LastUsed,
				Msid = loSessions[0].Msid,
			};

			return (UserSession != null);
		}

	}

	
}
