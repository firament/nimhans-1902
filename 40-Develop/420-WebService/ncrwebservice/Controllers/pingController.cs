﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ncrwebservice.Controllers
{
	/// <summary>
	/// General endpoint for testing and health checks.
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	public class pingController : ControllerBase
	{

		// Sample objects for reference.
		private void Resuklts()
		{
			IActionResult va;
			va = new Microsoft.AspNetCore.Mvc.BadRequestResult();           // 400
			va = new Microsoft.AspNetCore.Mvc.ConflictResult();             // 409
			va = new Microsoft.AspNetCore.Mvc.NoContentResult();            // 204
			va = new Microsoft.AspNetCore.Mvc.NotFoundResult();             // 404
			va = new Microsoft.AspNetCore.Mvc.OkResult();                   // 200
			va = new Microsoft.AspNetCore.Mvc.UnauthorizedResult();         // 401
			va = new Microsoft.AspNetCore.Mvc.UnprocessableEntityResult();  // 422
			va = new Microsoft.AspNetCore.Mvc.UnsupportedMediaTypeResult(); // 415
		}

		// GET api/values
		/// <summary>
		/// Ping test to availaibility of webservices.
		/// </summary>
		/// <returns>string array with current date and time information.</returns>
		[HttpGet]
		public ActionResult<IEnumerable<string>> Get()
		{
			DateTime ldtNow = DateTime.Now;
			return new string[] {
				  ldtNow.ToString("dddd")
				, ldtNow.ToString("MMMM")
				, ldtNow.ToString("yyyy-MM-dd")
				, ldtNow.ToString("HH:mm:ss.ffff")
				, ldtNow.ToString("zzz")
				};
		}

	}
}
