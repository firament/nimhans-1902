using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ncr.Efc;
using ncr.Efc.dbsets;
using ncr.Common;
using ncr.Common.Models;

namespace ncrwebservice.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	[Produces("application/json")]
	public class SignonController : NCR_Controller
	{
		
		public SignonController(ILogger<ScoresController> logger, NCR_DBContext db_context, IAppSettingContainer App_Config)
		: base(logger, App_Config, db_context)
		{ }

		#region AuthenticateUser
		/// <summary>
		/// Creates ticket against valid credentials.
		/// </summary>
		/// <param name="LoginCredentials">Container with data required for a successful signon</param>
		/// <returns>
		/// <para>Ticket for use in subsequent requests, if credentials valid</para>
		/// <para>Generic error if credentials are not valid.</para>
		/// </returns>
		/// <response code="400">Submitted form data failed validation.</response>
		/// <response code="204">Authentication failed for given user.</response>
		/// <response code="500">Error saving to underlying data. Inform system administrator.</response>
		/// <response code="200">OK. Authorization Success.</response>
		/// <remarks>Ticket will be expected for every subsequent request</remarks>
		[HttpPost]
		[ProducesErrorResponseType(typeof(Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary))]
		[ProducesResponseType(typeof(UserCreds), 400)]
		[ProducesResponseType(204)]
		[ProducesResponseType(500)]
		[ProducesResponseType(typeof(AuthTicket), 200)]
		public ActionResult<AuthTicket> AuthenticateUser([FromBody] UserCreds LoginCredentials)
		{
			// validate input
			if (
				   (!ModelState.IsValid)
				|| (LoginCredentials is null)
				|| (string.IsNullOrWhiteSpace(LoginCredentials.LOGIN))
				|| (string.IsNullOrWhiteSpace(LoginCredentials.PASSWORD))
				)
			{
				log.LogWarning("Validation failed for request.");
				return new BadRequestObjectResult(ModelState);
			}
			// TODO: Get only 1 active recruitment drive and add to query filter
			
			// Verify Credentials
			// f.RdId == LoginCredentials.RDID # Skip RDID check for now
			CommitteeMembers loUser = dbcontext.CommitteeMembers
							.Where(f
								=> f.Status == 1
								&& f.Login == LoginCredentials.LOGIN
								&& f.Password == LoginCredentials.PASSWORD
								)
							.FirstOrDefault();
			if (loUser == null) { return new NoContentResult(); }

			int liUserID = loUser.Userid;
			string lsToken = ncr.Common.Utils.Helper.Token();
			try
			{
				// Clear any stale or duplicate sessions for user
				List<MaintSessions> loSessions = dbcontext.MaintSessions
									.Where(f => f.Status == 1 && f.Userid == liUserID)
									.ToList();
				if (loSessions.Count > 0)
				{
					// ClearSessions()
					foreach (MaintSessions vMS in loSessions)
					{
						vMS.Status = 2;
					}
				}
				// create new ticket
				dbcontext.MaintSessions.Add(new MaintSessions()
				{
					Token = lsToken,
					DeviceSig = "",
					Userid = liUserID,
					Role = loUser.Role,
					IssueTime = DateTime.Now,
					Status = 1,
				});

				// Apply changes
				dbcontext.SaveChanges();
			}
			catch (System.Exception eX)
			{
				log.LogError(eX, $"Error authenticaating {LoginCredentials}");
				return StatusCode(500);
			}
			return Ok(new AuthTicket()
			{
				RDID = loUser.RdId,
				USERID = loUser.Userid,
				Role = loUser.Role,
				Name = loUser.Name,
				Ticket = lsToken
			});
		}
		#endregion // AuthenticateUser


		#region SignoutUser
		/// <summary>
		/// Cancel or Deactivate a valid session Ticket.
		/// </summary>
		/// <param name="Ticket">Authorization ticket to cancel.</param>
		/// <response code="400">Submitted form data failed validation. Ticket cannot be empty.</response>
		/// <response code="500">Error updating underlying data. Inform system administrator.</response>
		/// <response code="422">Ticket does not exist.</response>
		/// <response code="200">Ticket cancel is successful.</response>
		/// <returns></returns>
		[HttpDelete("{Ticket}")]
		[ProducesResponseType(400)]
		[ProducesResponseType(500)]
		[ProducesResponseType(422)]
		[ProducesResponseType(200)]
		public ActionResult SignoutUser(string Ticket)
		{
			if ((string.IsNullOrWhiteSpace(Ticket)))
			{
				log.LogWarning("Validation failed for request. No Ticket provided.");
				return new BadRequestObjectResult(ModelState);	// 400
			}
			try
			{
				List<MaintSessions> loSessions = dbcontext.MaintSessions
					.Where(f => f.Status == 1 && f.Token == Ticket)
					.ToList();
				if (loSessions.Count == 0){
					log.LogWarning("Aborting attempt to cancel non-existing ticket {0}", Ticket);
					return new UnprocessableEntityResult();
				}

				log.LogInformation("Found {0} Active sessions for Ticket {1}. Cancelling all without prejudice.", loSessions.Count, Ticket);
				// ClearSessions()
				foreach (MaintSessions vMS in loSessions)
				{
					vMS.Status = 2;
					log.LogInformation("Cancelling Ticket '{0}', issued to UserID '{1}' on '{2}', with Hash '{3}'", vMS.Token, vMS.Userid, vMS.IssueTime, vMS.DeviceSig);
				}
				dbcontext.SaveChanges();
			}
			catch (System.Exception eX)
			{
				log.LogError(eX, $"Error cancelling ticket '{Ticket}'");
				return StatusCode(500);
			}
			return Ok();
		}
		#endregion // SignoutUser


	}
}
