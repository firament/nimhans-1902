using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore;
using ncr.Efc;
using ncr.Efc.dbsets;
using ncr.Efc.DbFuncs;
using ncr.Common;
using ncr.Common.Models;
using ncr.Common.Constants;

namespace ncrwebservice.Controllers
{
	/// <summary>
	/// Generalized endpoint to fetch lists of entiies
	/// </summary>
	/// <remarks>Entries returned and Type of entries are controlled by value of ListFilter.listType</remarks>
	[Route("api/[controller]")]
	[Produces("application/json")]
	[ApiController]
	public class CandidateListController : NCR_Controller
	{
		#region Constructors
		public CandidateListController(ILogger<CandidateListController> logger, IAppSettingContainer App_Config, NCR_DBContext db_context)
		: base(logger, App_Config, db_context)
		{ }
		#endregion // Constructors

		#region POST
		/// <summary>
		/// Get list of Candidates, for Members View or Chairman view.
		/// </summary>
		/// <param name="ListFilter">
		/// Form with data specifying list type required, and aupporting data. 
		/// Field values have contextual meaning depending on value of <c>listType</c>
		/// 
		/// Valid values and usage for <c>ListFilter.listType</c> are: 
		/// 	10 = Candidates with Scores status marker (Feilds <c>memberLogin</c> and <c>memberName</c> will always be null), 
		/// 	20 = Candidates with Summarized scores and Grade marker (Feilds <c>memberLogin</c> and <c>memberName</c> will always be null), 
		/// 	30 = Candidates with Scores status marker, including scorer Name.
		/// </param>
		/// <returns>Valid values in system, of type requested by <c>ListFilter.listType</c></returns>
		/// <response code="400">Submitted form data failed validation.</response>
		/// <response code="401">Authorization Ticket failed check.</response>
		/// <response code="204">No results found for given criteria.</response>
		/// <response code="200">Success. Data requested will be in response.</response>
		/// <remarks>
		/// </remarks>
		[HttpPost]
		[ProducesResponseType(400)]
		[ProducesResponseType(typeof(ListForm), 401)]
		[ProducesResponseType(typeof(ListForm), 204)]
		[ProducesResponseType(typeof(List<string>), 200)]
		[ProducesResponseType(typeof(List<ncr.Common.Models.CandidateViewDetail>), 200)]
		public ActionResult GetResult([FromBody]ListForm ListFilter)
		{
			if (!ListFilter.IsValid())
			{
				return new BadRequestObjectResult(null);
			}
			if (!CheckSession(ListFilter.Ticket))
			{
				return new UnauthorizedResult();
			}
			(StatusCodeResult Status, List<CandidateViewDetail> DataList) CallT;
			switch (ListFilter.ListType)
			{

				case enListType.CandidatesIndividual:
					// CallT = CandidatesIndividual(UserSession.Userid, ListFilter.RDID, ListFilter.Department, ListFilter.PostAppliedFor);
					CallT = DbViews.CandidateListForMember(UserSession.Userid, appconfig, ListFilter);
					if (CallT.Status == null) { return Ok(CallT.DataList); }
					else { return CallT.Status; }

				case enListType.CandidatesSunmarized:
					CallT = DbViews.CandidateListForChairman(UserSession.Userid, UserSession.Role, appconfig, ListFilter);
					if (CallT.Status == null) { return Ok(CallT.DataList); }
					else { return CallT.Status; }

				case enListType.CandidatesScoreDetail:
					CallT = DbViews.CandidatesScoreDetail(UserSession.Userid, UserSession.Role, appconfig, ListFilter);
					if (CallT.Status == null) { return Ok(CallT.DataList); }
					else { return CallT.Status; }

				default:
					return new BadRequestObjectResult(ListFilter);

			}
		}
		#endregion // POST

		#region Helper Methods

		/* CODE RETIRED IN FAVOR OF USING RO CONNECTION
		/// <summary>
		/// List of Candidates for member, with flag to indicate if already scored.
		/// </summary>
		/// <param name="piUserID"></param>
		/// <param name="piRDID"></param>
		/// <param name="psDepartment"></param>
		/// <param name="psPost"></param>
		/// <returns></returns>
		[Obsolete("Use DbViews.CandidateListForMember")]
		private (StatusCodeResult Status, List<CandidateViewDetail> DataList) CandidatesIndividual(int piUserID, int piRDID, string psDepartment, string psPost)
		{
			StatusCodeResult loStatus = null;
			List<CandidateViewDetail> lstItems = new List<CandidateViewDetail>();
			decimal? liScore = 0;
			// DateTimeOffset? ldtScoreDate = null;
			try
			{
				// Code using efcore batch (single db round trip) - BEGIN
				List<ncr.Efc.dbsets.Candidates> llstCandScores = dbcontext.Candidates
						.Where(f => f.Status == 1 && f.RdId == piRDID && f.Department == psDepartment && f.PostApplied == psPost)
						.Include(r => r.Scores)
						.ToList()
						;
				log.LogInformation("Found {0} active Candidates", llstCandScores.Count);
				foreach (Candidates vCand in llstCandScores)
				{
					liScore = vCand.Scores.FirstOrDefault(f => f.Userid == piUserID)?.Score;
					lstItems.Add(
						new CandidateViewDetail()
						{
							RDID = (int)vCand.RdId,
							Code = vCand.Code,
							Name = vCand.Name,
							Age = vCand.Age, //(vCand.Age.HasValue) ? ((System.DateTime)vCand.Age).ToString("yy MM dd") : "null",
							Category = vCand.Category,
							Score = (liScore.HasValue) ? liScore.Value : 0,
							ScoreCount = vCand.Scores.Count,
							Grade = string.Empty,
							IsDone = liScore.HasValue,
						}
					);
				}
				if (lstItems.Count == 0) loStatus = new NoContentResult();
				// Code using efcore batch (single db round trip) - END

				/*
				// Code using Manual query - BEGIN
				List<Candidates> loCands = dbcontext.Candidates
						.Where(f => f.RdId == piRDID && f.Department == psDepartment && f.PostApplied == psPost).ToList();
				List<ncr.Efc.dbsets.Scores> loScores = dbcontext.Scores
						.Where(f => f.RdId == piRDID && f.Userid == piUserID).ToList();

				log.LogInformation("Found {0} active Posts", lstItems.Count);
				foreach (Candidates vCand in loCands)
				{
					// ncr.Efc.dbsets.Scores loScore = loScores.Find(f => f.Code == vCand.Code);
					// liScore = loScore?.Score;
					// ldtScoreDate = loScore?.EditOn;
					liScore = loScores.Find(f => f.Code == vCand.Code)?.Score;
					lstItems.Add(
						new CandidateViewDetail()
						{
							RDID = (int)vCand.RdId,
							Code = vCand.Code,
							Name = vCand.Name,
							Age = vCand.Age, //(vCand.Age.HasValue) ? ((System.DateTime)vCand.Age).ToString("yy MM dd") : "null",
							Category = vCand.Category,
							Score = (liScore.HasValue) ? liScore.Value : 0,
							//ScoreDate = (ldtScoreDate.HasValue) ? ((System.DateTime)ldtScoreDate).ToString("dd/MMM/yy") : "na",
							Grade = string.Empty,
							IsDone = liScore.HasValue,
						}
					);
				}
				if (lstItems.Count == 0) loStatus = new NoContentResult();
				// Code using Manual query - END
				* /
			}
			catch (System.Exception ex)
			{
				log.LogError(ex, "Error retrieving Posts.");
				loStatus = StatusCode(500);
			}
			return (loStatus, lstItems);
		}
		*/

		#endregion //Helper Methods

	}
}
