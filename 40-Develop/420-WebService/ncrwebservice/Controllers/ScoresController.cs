using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ncr.Efc;
using ncr.Efc.dbsets;
using ncr.Common;
using ncr.Common.Models;
using ncr.Common.Constants;

namespace ncrwebservice.Controllers
{
	/// <summary>
	/// Post Scores, Grades and View summarized scores.
	/// </summary>
	[Route("api/[controller]")]
	[ApiController]
	[Produces("application/json")]
	public class ScoresController : NCR_Controller
	{

		public ScoresController(ILogger<ScoresController> logger, NCR_DBContext db_context, IAppSettingContainer App_Config)
		: base(logger, App_Config, db_context)
		{ }

		#region PUT CandidateScore
		/// <summary>
		/// Post Score or Grade for a candidate.
		/// </summary>
		/// <param name="ScoreGrade">
		/// Data submission container. 
		/// 
		/// Valid values and usage for <c>ScoreGrade.listType</c> are: 
		/// 	100 = Only Score is to be updated, 
		/// 	200 = Only Grade is to be updated, 
		/// 	300 = Both Score and Grade are to be updated.
		/// </param>
		/// <returns>OK 200 on successful update.</returns>
		/// <response code="400">Submitted form data failed validation.</response>
		/// <response code="401">Authorization Ticket failed check.</response>
		/// <response code="409">Bad data, inform application administrator to fix data.</response>
		/// <response code="422">Attempt to re-post failed. Scores and Grades cannot be edited.</response>
		/// <response code="500">Error saving to underlying data. Inform system administrator.</response>
		/// <response code="200">Post is successful.</response>
		/// <remarks>
		/// </remarks>
		[HttpPut()]
		[ProducesErrorResponseType(typeof(Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary))]
		[ProducesResponseType(409)]
		[ProducesResponseType(typeof(UserCreds), 401)]
		[ProducesResponseType(409)]
		[ProducesResponseType(500)]
		[ProducesResponseType(200)]
		public IActionResult CandidateScore([FromBody] ScoreForm ScoreGrade)
		{
			if ((!ModelState.IsValid) || (ScoreGrade == null) || !ScoreGrade.IsValid())
			{
				return new BadRequestObjectResult(ModelState);
			}
			// Ensure valid session
			if (
				   !CheckSession(ScoreGrade.Ticket)
				|| ((ScoreGrade.ListType == enListType.GradeOnly || ScoreGrade.ListType == enListType.BothScoreAndGrade) && UserSession.Role != 2)
				)
			{
				return new UnauthorizedResult();
			}

			// PROCESS SCORES
			if (ScoreGrade.ListType == enListType.ScoreOnly || ScoreGrade.ListType == enListType.BothScoreAndGrade)
			{ // Post Score
				List<Scores> loItems;
				loItems = dbcontext.Scores
						.Where(f => f.Status == 1
								 && f.RdId == ScoreGrade.RDID
								 && f.Code == ScoreGrade.Code
								 && f.Userid == UserSession.Userid
								 )
						.ToList();

				if (loItems.Count > 1)
				{
					return new ConflictResult();
				}
				Scores loScore;
				if (loItems.Count == 1)
				{
					loScore = loItems[0];
					if (loScore.Score > 0 && UserSession.Role != 2)	// Chairman can override score
					{
						return new UnprocessableEntityResult();
					}
				}
				else
				{
					loScore = new Scores()
					{
						RdId = ScoreGrade.RDID,
						Userid = UserSession.Userid,
						Code = ScoreGrade.Code,
						Status = 1,
						AddBy = UserSession.Userid,
						EditBy = UserSession.Userid,
						AddOn = DateTime.Now,
						EditOn = DateTime.Now
					};
					dbcontext.Add(loScore);
				}
				loScore.Score = ScoreGrade.Score;
				loScore.EditBy = UserSession.Userid;
				loScore.EditOn = DateTime.Now;
			}

			// PROCESS GRADES
			if (
				(ScoreGrade.ListType == enListType.GradeOnly || ScoreGrade.ListType == enListType.BothScoreAndGrade)
				&& UserSession.Role == 2
				)
			{ // Post Grade
				List<Grades> loItems;
				loItems = dbcontext.Grades
						.Where(f => f.Status == 1
								 && f.RdId == ScoreGrade.RDID
								 && f.Code == ScoreGrade.Code
								 && f.Userid == UserSession.Userid
								 )
						.ToList();

				if (loItems.Count > 1)
				{
					return new ConflictResult();
				}
				Grades loGrade;
				if (loItems.Count == 1)
				{
					loGrade = loItems[0];
					if (!string.IsNullOrWhiteSpace(loGrade.Grade1) && UserSession.Role != 2)    // Chairman can override grade
					{
						return new UnprocessableEntityResult();
					}
				}
				else
				{
					loGrade = new Grades()
					{
						RdId = ScoreGrade.RDID,
						Code = ScoreGrade.Code,
						Userid = UserSession.Userid,
						Status = 1,
						AddBy = UserSession.Userid,
						EditBy = UserSession.Userid,
						AddOn = DateTime.Now,
						EditOn = DateTime.Now
					};
					dbcontext.Add(loGrade);
				}
				loGrade.Grade1 = ScoreGrade.Grade;
				loGrade.EditBy = UserSession.Userid;
				loGrade.EditOn = DateTime.Now;
			}

			// SAVE CHANGES TO DB
			try
			{
				dbcontext.SaveChanges();
			}
			catch (System.Exception eX)
			{
				log.LogError(eX, "Error updating Score/Grade");
				return StatusCode(500);
			}
			return Ok();
		}
		#endregion // PUT CandidateScore

		#region GetScoreSummary
		/// <summary>
		/// Get detailed Scores and Grade.
		/// </summary>
		/// <returns></returns>
		/// <response code="500">GET Method is not yet implemented.</response>
		/// <remarks>Do NOT use, for future enhancement.</remarks>
		[HttpGet]
		[ProducesErrorResponseType(typeof(Microsoft.AspNetCore.Mvc.ModelBinding.ModelStateDictionary))]
		[ProducesResponseType(typeof(string), 500)]
		[ProducesResponseType(typeof(UserCreds), 404)]
		[ProducesResponseType(typeof(List<CandidateViewDetail>), 200)]
		public ActionResult<List<CandidateViewDetail>> GetScoreSummary()
		{
			NotImplementedException lNIE = new NotImplementedException("This Method isnot yet implemented");
			log.LogCritical(lNIE, "Method 'GetScoreSummary' is not yet implemented");
			return StatusCode(500, "GET Method is not yet implemented.");
		}
		#endregion // GetScoreSummary



	}
}
