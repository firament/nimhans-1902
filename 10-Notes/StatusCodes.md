Microsoft.AspNetCore.Mvc.StatusCodeResult
https://docs.microsoft.com/en-us/dotnet/api/microsoft.aspnetcore.mvc.statuscoderesult?view=aspnetcore-2.2

va = new Microsoft.AspNetCore.Mvc.BadRequestResult();
va = new Microsoft.AspNetCore.Mvc.ConflictResult();
va = new Microsoft.AspNetCore.Mvc.NoContentResult();
va = new Microsoft.AspNetCore.Mvc.NotFoundResult();
va = new Microsoft.AspNetCore.Mvc.OkResult();
va = new Microsoft.AspNetCore.Mvc.UnauthorizedResult();
va = new Microsoft.AspNetCore.Mvc.UnprocessableEntityResult();
va = new Microsoft.AspNetCore.Mvc.UnsupportedMediaTypeResult();
va = new System.Web.Http.ConflictResult();
va = new System.Web.Http.InternalServerErrorResult();
