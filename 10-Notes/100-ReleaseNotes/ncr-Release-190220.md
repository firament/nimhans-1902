# Release 2019 Feb 20

## Changes
- [x] Defect - Age values not correct for Chairman list
- [x] Include all possible fields in result container
  - , regardless of need
  - , and to allow maximum flexibility in reporting.

## Environment Update Instructions
1. From attachment, run database scripts in beolw sequence
   1. db-30-drop-views.sql
   2. db-21-add-views.sql
2. Download file `NCR_WebService-*yymmdd*.zip` 
   1. and follow steps in 'Update Webservice' section.
